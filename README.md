## 介绍

zcm udp模块跨主机docker模块通信demo

### 使用

1. 获取镜像

   ```
   git clone https://gitlab.com/ningfeng666/zcmindocker_udp_demo.git
   cd zcmindocker_udp_demo
   docker build -t littlehuang/zcm_udp_demo .
   ```

   或者

   ```
   docker pull littlehuang/zcm_udp_demo
   ```

2. 测试

   ```sh
   # 主机1执行
   docker run --net host littlehuang/zcm_udp_demo
   # 主机2执行
   docker run --net host -it littlehuang/zcm_udp_demo /bin/bash
   ./hostB
   ```

   

