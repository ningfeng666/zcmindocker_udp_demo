#include <stdio.h>
#include <unistd.h>
#include <string>
#include <zcm/zcm-cpp.hpp>
#include "HELLO.hpp"
using std::string;

class Handler
{
    public:
        ~Handler() {}

        void handleMessage(const zcm::ReceiveBuffer* rbuf,
                           const string& chan,
                           const HELLO *msg)
        {
                printf("Received a message on channel '%s'\n", chan.c_str());
                printf("msg->str = '%s'\n", msg->str.c_str());
        }
};

int main(int argc, char *argv[])
{
    zcm::ZCM zcm {"udpm://239.255.76.67:7667?ttl=1"};
    if (!zcm.good())
        return 1;

    Handler handlerObject;
    zcm.subscribe("TEST", &Handler::handleMessage, &handlerObject);
    zcm.run();

    return 0;
}
