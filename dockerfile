FROM zerocm/zcm
MAINTAINER littleHuang<ysh626.std@gmail.com>
ENV MYPATH /zcm_trans
COPY cpp_demo/ $MYPATH/cpp_demo/
ENV LD_LIBRARY_PATH /usr/lib:/usr/local/lib
ENV ZCM_DEFAULT_URL udpm://239.255.76.67:7667?ttl=1
WORKDIR $MYPATH/cpp_demo
RUN mkdir build&& g++ hostA.cpp -std=c++11 -lzcm -o build/hostA && g++ hostB.cpp -std=c++11 -lzcm -o build/hostB
EXPOSE 7667
WORKDIR $MYPATH/cpp_demo/build
CMD ./hostA